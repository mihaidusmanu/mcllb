import numpy as np
import itertools as itert
from sklearn import mixture
from sklearn import metrics

speakers = ['b', 'g', 'm', 'w']
vowels = ["ae", "ah", "aw", "eh", "er", "ei", "ih", "iy", "oa", "oo", "uh", "uw"]

def get_position(x, v): 
    for i in range(len(v)):
        if v[i] == x:
            return(i)
    raise NameError("Element not found")

def get_dur_F1_F2_F3():
    f = open("./Data/vowels-fs.dat", "r")
    raw_data = f.read()
    data = raw_data.splitlines()
  
    s = [[[] for j in range(len(vowels))] for i in range(len(speakers))]
    for raw_entry in data:
        entry = raw_entry.split()
        speaker_id = get_position(entry[0][0], speakers)
        vowel_id = get_position(entry[0][3:5], vowels)
        #dur F1 F2 F3
        current_sample = np.array([int(entry[1]), int(entry[3]), int(entry[4]), int(entry[5])])
        if speaker_id == 2 and current_sample[1] != 0 and current_sample[2] != 0 and current_sample[3] != 0:
             s[speaker_id][vowel_id].append(current_sample)
  
    sf = np.array([[np.array(s[i][j]) for j in range(len(vowels))] for i in range(len(speakers))])
    return(sf) 

def select(samples, subset):
    s = []
    exp = []

    for speaker_id in range(len(speakers)):
        for vowel_id in range(len(vowels)):
            for entry in samples[speaker_id][vowel_id]:
                m_entry = []
                for i in subset:
                    m_entry.append(entry[i])
                s.append(np.array(m_entry))
                exp.append(vowel_id)

    s = np.array(s)
    exp = np.array(exp)
 
    return (s, exp)

def test(samples):
    r = range(4)
            
    for current_length in range(1, len(r) + 1):
        for subset in itert.combinations(r, current_length):
            s, expected = select(samples, subset)
            gmm = mixture.GMM(n_components = len(vowels), covariance_type = "full", n_iter = 100)
            gmm.fit(s)
            pred = gmm.predict(s)
            print(subset, "->", metrics.adjusted_rand_score(expected, pred))
    
    return() 

test(get_dur_F1_F2_F3())
