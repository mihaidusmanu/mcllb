import wave
import struct
import numpy as np
import spectral
import itertools as itert
from sklearn import mixture
from sklearn import metrics
from math import ceil, floor

prefix = "./Sounds/"
speakers = ["b", "g", "m", "w"]
vowels = ["ae", "ah", "aw", "eh", "er", "ei", "ih", "iy", "oa", "oo", "uh", "uw"]

def get_position(x, v):
    for i in range(len(v)):
        if v[i] == x:
            return(i)
    raise NameError("Element not found")

def process(filename, stab1, stab2):
    with wave.open(prefix + filename + ".wav", "r") as fid:
        _, _, fs, nframes, _, _ = fid.getparams()
        sig = np.array(struct.unpack_from("%dh" % nframes, fid.readframes(nframes)))
    
    config = dict(fs = fs, scale = 'mel', deltas = False)
    extractor = spectral.Spectral(**config)
    cepstra = extractor.transform(sig)
    #return((cepstra[floor(stab1 / 10)] + cepstra[floor(stab2 / 10)]) / 2)
    return(cepstra[floor((stab1 - 10) / 10)]) 

def get_data():
    f = open(prefix + "timedata.dat", "r")
    raw_data = f.read()
    data = raw_data.splitlines()

    s = []
    expected = []
    for raw_entry in data:
        entry = raw_entry.split()
        if entry[0][0] == 'm':
            s.append(process(entry[0], float(entry[3]), float(entry[4])))
            expected.append(get_position(entry[0][3:5], vowels))

    s = np.array(s)
    expected = np.array(expected)
    return(s, expected)

def fit_GMM(samples, expected):
    gmm = mixture.GMM(n_components = len(vowels), covariance_type = "full")
    gmm.fit(samples)
    pred = gmm.predict(samples)
    print(metrics.adjusted_rand_score(expected, pred))
    
    return() 

data = get_data()
fit_GMM(data[0], data[1])
