import numpy as np
import itertools as itert
from sklearn import mixture
from sklearn import metrics

speakers = ['b', 'g', 'm', 'w']
vowels = ["ae", "ah", "aw", "eh", "er", "ei", "ih", "iy", "oa", "oo", "uh", "uw"]

def get_position(x, v): 
    for i in range(len(v)):
        if v[i] == x:
            return(i)
    raise NameError("Element not found")

def verif(sample):
    for i in range(1, len(sample)):
        if sample[i] == 0:
            return(False)
    return(True)

def get_full_data():
    f = open("./Data/vowels-fs.dat", "r")
    raw_data = f.read()
    data = raw_data.splitlines()
  
    s = []
    expected = []
    for raw_entry in data:
        entry = raw_entry.split()
        speaker_id = get_position(entry[0][0], speakers)
        vowel_id = get_position(entry[0][3:5], vowels)
        current_sample = np.array([int(entry[1])] + [int(entry[i]) for i in range(3, len(entry))])
        if speaker_id == 2 and verif(current_sample):
             s.append(current_sample)
             expected.append(vowel_id)
  
    sf = np.array(s)
    expectedf = np.array(expected)
    return(sf, expectedf)

def test(samples, expected):
    gmm = mixture.GMM(n_components = len(vowels), covariance_type = "full", n_iter = 100)
    gmm.fit(samples)
    pred = gmm.predict(samples)
    print(metrics.adjusted_rand_score(expected, pred))
    
    return() 

data = get_full_data()
test(data[0], data[1])
