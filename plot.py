import itertools
import numpy as np
import math
from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn import mixture
from matplotlib.colors import LogNorm

speakers = ['b', 'g', 'm', 'w']
vowels = ["ae", "ah", "aw", "eh", "er", "ei", "ih", "iy", "oa", "oo", "uh", "uw"]
colors = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF", "#00FFFF", "#800000", "#008000", "#000080", "#808000", "#800080", "#008080"]

def get_position(x, v): 
    for i in range(len(v)):
        if v[i] == x:
            return(i)
    raise NameError("Element not found")

def get_F1_F2():
    f = open("./Data/vowels-fs.dat", "r")
    raw_data = f.read()
    data = raw_data.splitlines()
  
    s = [[[] for j in range(len(vowels))] for i in range(len(speakers))]
    for raw_entry in data:
        entry = raw_entry.split()
        speaker_id = get_position(entry[0][0], speakers)
        vowel_id = get_position(entry[0][3:5], vowels)
        current_sample = np.array([int(entry[3]), int(entry[4])])
        if current_sample[0] != 0 and current_sample[1] != 0:
            s[speaker_id][vowel_id].append(current_sample)
  
    sf = np.array([[np.array(s[i][j]) for j in range(len(vowels))] for i in range(len(speakers))])
    return(sf)  

def create_ellipse(mean, covar, col):
    v, w = linalg.eigh(covar)
    angle = np.arctan2(w[0][1], w[1][1]) #angle of the eigenvector that corresponds to the largest eigenvalue
    angle = 180 * angle / np.pi
    ell = mpl.patches.Ellipse(mean, 2 * np.sqrt(v[0] * 5.991), 2 * math.sqrt(v[1] * 5.991), -angle, color = col) # 95% confidence interval
    ell.set_alpha(0.1)
    return(ell)

def plot_vowels(samples):
    for  vowel_id in range(len(vowels)):
        s = samples[0][vowel_id][:]
        for speaker_id in range(1, len(speakers)):
            s = np.concatenate((s, samples[speaker_id][vowel_id][:]), axis = 0)
        col = colors[vowel_id]
        plt.scatter(s[:, 0], s[:, 1], alpha = 0.5, color = col, s = 5)
        plt.gcf().gca().add_artist(create_ellipse(s.mean(0), np.cov(np.array([s[:, 0], s[:, 1]])), col)) 
  
    plt.xlabel("F1")
    plt.ylabel("F2")
    plt.show()
    
    return()

def plot_components(s, gmm):
    means = gmm.means_
    pred = gmm.predict(s)
    
    mapped = [[] for i in range(len(vowels))]
    for id in range(len(s)):
        mapped[pred[id]].append(s[id])
    
    for vowel_id in range(len(vowels)):
        mapped[vowel_id] = np.array(mapped[vowel_id])
    for vowel_id in range(len(vowels)):
        if len(mapped[vowel_id]) != 0:
            plt.plot(mapped[vowel_id][:, 0], mapped[vowel_id][:, 1], ".", alpha = 0.5, color = colors[vowel_id])
            plt.plot(means[vowel_id][0], means[vowel_id][1], "s", color = colors[vowel_id])
    
    return()

def plot_ellipses(gmm):
    means = gmm.means_
    covars = gmm._get_covars()

    for id, (mean, covar) in enumerate(zip(means, covars)):
        plt.gcf().gca().add_artist(create_ellipse(mean, covar, colors[id]))
    
    return()

def plot_GMM(samples):
    s = None
    for speaker_id in range(len(speakers)):
        for vowel_id in range(len(vowels)):
            if s is None:
                s = samples[speaker_id][vowel_id]
            else:
                s = np.concatenate([s, samples[speaker_id][vowel_id]], axis = 0)
    
    gmm = mixture.GMM(n_components = len(vowels), covariance_type = "full", n_iter = 100)
    gmm.fit(s)
    plot_components(s, gmm)
    plot_ellipses(gmm)

    plt.show()

    return()

#plot_vowels(get_F1_F2())
plot_GMM(get_F1_F2())
